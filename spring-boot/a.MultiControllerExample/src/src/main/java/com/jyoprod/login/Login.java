package com.jyoprod.login;

public class Login {

    private final long loginID;
	private final String content;

    public Login(long id, String content) {
        this.loginID = id;
        this.content = content;
    }

   
    public long getLoginID() {
		return loginID;
	}
    
    public String getContent() {
        return content;
    }
}
