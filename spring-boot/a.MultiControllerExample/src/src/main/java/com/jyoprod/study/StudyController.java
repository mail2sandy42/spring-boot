package com.jyoprod.study;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudyController {
	
	// do http get usign http://localhost:8080/study/count
	@RequestMapping("/study/count")
	public int StudyCount(){
		return 5;
	}

	//do a http get with this URL http://localhost:8080/study/count/Sandeep
	@RequestMapping(value = "/study/count/{patientName}", method = RequestMethod.GET)
	public String StudyCount(@PathVariable("patientName") String patientName){
		return "Patient " + patientName + " has " + 5 + " studies";
	}
}
