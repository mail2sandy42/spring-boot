package com.jyoprod.login;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.models.Employee;
import com.utility.EmployeeHelper;

@RestController
public class LoginController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	// to call use postman and perform get with URL -
	// http://localhost:8080/login?userName=Sandeep&password=san
	@RequestMapping("/login")
	public Login greeting(@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "password", required = true) String password) {
		return new Login(counter.incrementAndGet(), String.format(template, userName));
	}

	@RequestMapping(value = "/employees/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getEmployee(
			@PathVariable("id") String empId,
			@RequestParam(value = "fields", required = false) String[] fieldFilter) {
		
	
		Employee emp = EmployeeHelper.GenerateAnEmployeeFor(empId);
		
	
		Map<String, Object> hmap = null;
		try {
			hmap = EmployeeHelper.ConvertObjectToMap(emp, fieldFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return hmap;

	}

}
