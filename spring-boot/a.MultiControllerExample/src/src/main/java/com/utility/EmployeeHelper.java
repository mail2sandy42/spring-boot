package com.utility;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.models.Employee;

public class EmployeeHelper {

	public static Employee GenerateAnEmployeeFor(String empId) {
		Employee em = new Employee();
		em.setFirstName("Employee" + empId);
		em.setLastName("Kumar");
		em.setCurrentCity("City" + empId);
		em.setId(empId);
		em.setBithCity("OldStreet" + empId);
		em.setDateOfBirth("01-01-1990");
		return em;
	}

	public static Map<String, Object> ConvertObjectToMap(Object obj, String[] fieldFilter)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		Method[] methods = obj.getClass().getMethods();

		Map<String, Object> map = new HashMap<String, Object>();
		for (Method m : methods) {

			if (m.getName().startsWith("get") && !m.getName().startsWith("getClass")) {
				String propName = m.getName().substring(3);
				if (fieldFilter != null && containsIgnoreCase(fieldFilter,propName) == false)
					continue;
				Object value = (Object) m.invoke(obj);
				map.put(propName, (Object) value);
			}
		}
		return map;
	}

	private static boolean containsIgnoreCase(String[] allEntries, String specificItem) {
		List<String> list = Arrays.asList(allEntries);
		for (String current : list) {
			if (current.equalsIgnoreCase(specificItem)) {
				return true;
			}
		}
		return false;
	}

}
