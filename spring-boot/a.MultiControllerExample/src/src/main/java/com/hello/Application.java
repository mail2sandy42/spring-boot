package com.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.jyoprod.login.LoginController;
import com.jyoprod.study.StudyController;

@SpringBootApplication
@ComponentScan(basePackageClasses = {LoginController.class,StudyController.class})//You can just use @ComponentScan and avoid explicitly add each controller
public class Application {
   public static void main( String[] args ) {
      SpringApplication.run( Application.class, args );
   }
}