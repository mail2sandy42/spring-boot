##This is a simple basic example which shows below things:
- How to map multiple controllers in @ComponentScan
- Usage of @RestController for your controller class 
- Basic pom configuration for spring boot application 
- **Querying partial object**


##How to use:

-Login
```
http://localhost:8080/login?userName=Sandeep&password=san
```

-Query a employee object
```
http://localhost:8080/employees/2
```

-Query only ID and name of employee
```
http://localhost:8080/employees/2?fields=firstName,id
```

