package com.jyoproducts;

import org.springframework.data.annotation.Id;

///This class holds the structure that will be stored in DB.
public class Tagging {

	@Id
	private String id;

	private String deviceID;
	private String staticData;
	private String blobData;//added for learning purpose
	
	public String getStaticData() {
		return staticData;
	}

	public void setStaticData(String staticData) {
		this.staticData = staticData;
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getBlobData() {
		return blobData;
	}

	public void setBlobData(String blobData) {
		this.blobData = blobData;
	}

}