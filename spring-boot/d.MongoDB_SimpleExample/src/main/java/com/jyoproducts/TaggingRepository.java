package com.jyoproducts;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource( collectionResourceRel = "tagging", path = "tagging" )
public interface TaggingRepository extends MongoRepository<Tagging, String> {

   List<Tagging> findByDeviceID( @Param( "name" ) String name );

   List<Tagging> findByStaticData( @Param( "name" ) String name );

}