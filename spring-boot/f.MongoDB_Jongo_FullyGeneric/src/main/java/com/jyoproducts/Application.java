package com.jyoproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.jyoproducts.controllers.EmployeesController;

@SpringBootApplication
@ComponentScan(basePackageClasses = {EmployeesController.class})
public class Application {
   public static void main( String[] args ) {
      SpringApplication.run( Application.class, args );
   }
}