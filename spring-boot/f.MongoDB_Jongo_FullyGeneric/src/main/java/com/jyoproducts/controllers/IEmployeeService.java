package com.jyoproducts.controllers;

import java.util.List;

import com.jyoproducts.model.Employee;

public interface IEmployeeService {

	Boolean AddEmployee(Employee employee);

	List<Employee> GetEmployeeList(String[] filterzip, String[] filterCity);

	List<Employee> GetEmployeeList(String mongoShellQuery);

}