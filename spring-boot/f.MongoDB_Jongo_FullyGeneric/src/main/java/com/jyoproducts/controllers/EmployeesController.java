package com.jyoproducts.controllers;


import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jyoproducts.model.Employee;



@RestController
@RequestMapping("Employees")
public class EmployeesController extends ControllerBase {

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Boolean AddEmployee(@RequestBody Employee employee) {
		return employeeService.AddEmployee(employee);
	}
	
	 @RequestMapping(value = "/", method = RequestMethod.GET)
	    public @ResponseBody List<Employee> GetEmployeeList(
	    		@RequestParam(value = "filter-zipcode", required = false) String[] filterzip,
	    		@RequestParam(value = "filter-city", required = false) String[] filterCity,
	    		@RequestParam(value = "filterAddress", required = false) String filterAddress) {
	      /* filter-zipcode and filter-city has been retained for the sake of demonstration.
	       * Both zipcode and city are part of the blob field. So this should be done using the request body*/
		 if(filterAddress == null)
	    	return employeeService.GetEmployeeList(filterzip,filterCity);
		 else
			 return employeeService.GetEmployeeList(filterAddress);
	    }
}
