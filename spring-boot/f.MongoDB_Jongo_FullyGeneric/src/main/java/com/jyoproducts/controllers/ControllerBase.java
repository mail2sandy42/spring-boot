package com.jyoproducts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;



public class ControllerBase {

	@Autowired
	protected IEmployeeService employeeService;
	
	
	@Bean
	public IEmployeeService studyService() {
		return new EmployeeServiceImpl();
	}
	
}
