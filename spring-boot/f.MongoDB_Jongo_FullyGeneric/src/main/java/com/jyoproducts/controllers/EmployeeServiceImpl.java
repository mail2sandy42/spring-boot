package com.jyoproducts.controllers;

import java.util.ArrayList;
import java.util.List;

import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.springframework.beans.factory.annotation.Value;

import com.jyoproducts.model.Employee;
import com.mongodb.DB;
import com.mongodb.MongoClient;

public class EmployeeServiceImpl implements IEmployeeService {

	@Value("${spring.data.mongodb.port}")
	private int port;

	@Value("${spring.data.mongodb.host}")
	private String host;

	@Value("${employee.app.dbname}")
	private String dbName;

	@Value("${employee.app.collection}")
	private String collName;

	@Override
	public Boolean AddEmployee(Employee employee) {
		DB db;
		try {

			MongoClient mongo = new MongoClient(host, port);
			db = mongo.getDB(dbName);
			Jongo jongo = new Jongo(db);
			MongoCollection allEmps = jongo.getCollection(collName);
			allEmps.insert(employee);
			mongo.close();
			return true;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public List<Employee> GetEmployeeList(String[] filterzip, String[] filtercity) {
		String qry = GetQueryString(filterzip,filtercity);
		return GetEmployeeList(qry);
	}

	@Override
	public List<Employee> GetEmployeeList(String mongoShellQuery) {
		DB db;
		List<Employee> res = new ArrayList<Employee>();
		try {

			MongoClient mongo = new MongoClient(host, port);
			db = mongo.getDB(dbName);
			Jongo jongo = new Jongo(db);
			MongoCollection allEmps = jongo.getCollection(collName);
			

			MongoCursor<Employee> all = allEmps.find(mongoShellQuery).as(Employee.class);
			if(all != null){
				while (all.hasNext()) {
					res.add(all.next());
				}
			}
			
			
			mongo.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	
	private String GetQueryString(String[] filterzip, String[] filtercity) {
		/*db.getCollection('AllEmployees').find({
	    $or: [ { "address.zipcode": "99" }, { "address.zipcode": "567" } ] ,
	    "address.city" : "mysore"
	    })*/
		List<String> resList = new ArrayList<String>();
		resList.add(ConstructQuery(filterzip,"address.zipcode",false));
		//you might have constructed - 
		//$or: [ { "address.zipcode": "99" }, { "address.zipcode": "567" } ] 
		
		resList.add(ConstructQuery(filtercity,"address.city",false));
		
		String finalRes = "{";
		for(int i=0;i<resList.size();i++){
			if(resList.get(i).isEmpty()==false){

				finalRes += resList.get(i) + ",";
			}
		}

		finalRes = finalRes.substring(0,finalRes.length()-1) + "}";
		return finalRes;
	}


	private String ConstructQuery(String[] inputArgs, String field,boolean isNumber) {
		
		if (inputArgs == null)
			return "";
		if(inputArgs.length == 0)
			return "";
		
		
		String format;
		if(isNumber)
			format = "\"%s\": %s";
		else
			format = "\"%s\": \"%s\"";
		
		if(inputArgs.length == 1)
			return String.format(format,field,inputArgs[0]);

		format = "{\"%s\": \"%s\"}";
		
		String res = "[";
		
		for(int i=0;i<inputArgs.length;i++){
			res += String.format(format,field,inputArgs[i]) + ",";
		}
		res = res.substring(0,res.length()-1) + "]";
		return "$or:" + res;
	}

	

}
