##99technest ![alt text](http://www.99technest.com/images/logo.png)
####Description of the example:
This is a generic solution where blob can be treated as querable object in mongo.
Jongo has been used to execute mongo-shell queries in java.

####Key take away:
* Externalized Configuration (like DBname,Collection name etc) in application.properties. 
* Querying from blob fields.
* Passing object in request body.
* Passing filter arguments as optional parameter.
* Constructing mongo-shell query in a generic way.
* Usage of [jongo](http://jongo.org).

##How to use (Also demonstrates design approach):

###Usage 1: Predefined filter arguments
- First add some dummy data in the format shown below.

```
Perform http POST :localhost:8082/Employees/
```

	body as shown below
	{
    "id" : "id1",
    "name" : "user1",
    "address" : {"zipcode" : "1234","city" : "meersburg"}
	}
	
- With many such objects, perform a query like below.
Here filter-fieldName will accept comma seperated multi inputs. 
Below query will get employees who belong to meersburg city and whose zipcode is either 99 or 567.

```
localhost:8082/Employees/?filter-zipcode=99,567&filter-city=meersburg
```
Internally, this will generate a query like below
```
db.getCollection('AllEmployees').find({
	    $or: [ { "address.zipcode": "99" }, { "address.zipcode": "567" } ] ,
	    "address.city" : "meersburg"
	    })
```	   
####Advantages of this approach:
- Works well if the model(schema) is already known. Meaning, If address is a well defined model *(and not a blob)*, then you will know for sure that there is a field called "zipcode" and it is a integer or string.
 
####Limitations of this approach:
- It is assumed that the blob field has a attribute called "zipcode".
- Assumption is extended to think that zipcode is a string.
- Parameter "filter-zipcode" is pre-defined in the GET as @RequestParam. If address has StreetNumber then new @RequestParam has to be created.

------------------------------------

###Usage 2: Generic query parameter
####Description:
If it is blob we will not know whether it is zipcode or zip or some other name. Also, mongo-shell query for integer and string are different. We cannot go with the assumption that zipcode is always string.
This approach overcomes those limitations. This can be adopted when the quires are dealing only with blob fields.

####What is the difference:
Instead of using "filter-zipcode" or "filter-city", a query parameter is available as "filterAddress". Here you can pass any mongo-shell query which satisfies the conditions. Below is the URL for quering employees who are belong to meersburg city and whose zipcode is either 99 or 567.

```
localhost:8082/Employees/?filterAddress={ $or: [ { "address.zipcode": "99" }, { "address.zipcode": "567" } ] ,     "address.city" : "meersburg" }
```

####Limitations:
This service will find it hard to migrate to other database as "filterAddress" will directly accept mongo-shell query. But then, you are dealing with blob and mongo is a very neat solution for such requirement.
