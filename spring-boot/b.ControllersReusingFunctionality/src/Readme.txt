This is same as example "multiController". 
- All functionalities are provided by a controllerBase.
- Other controllers are merely re-using the base functionality.
- Next step will be to introduce abstration and inject the concrete implementation.
