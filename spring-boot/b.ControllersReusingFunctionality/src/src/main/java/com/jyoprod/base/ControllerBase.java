package com.jyoprod.base;

public class ControllerBase {
	
	protected int getAllStudyCount(){
		return 5;
	}
	protected String getStudyCountFor(String patientName){
		return "Patient " + patientName + " has " + 5 + " studies";
	}
}
