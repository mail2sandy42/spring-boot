package com.jyoprod.study;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jyoprod.base.ControllerBase;

@RestController
public class StudyController extends ControllerBase{
	
	// do http get usign http://localhost:8080/study/count
	@RequestMapping("/study/count")
	public int studyCount(){
		return getAllStudyCount();
	}

	//do a http get with this URL http://localhost:8080/study/count/Sandeep
	@RequestMapping(value = "/study/count/{patientName}", method = RequestMethod.GET)
	public String studyCount(@PathVariable("patientName") String patientName){
		return getStudyCountFor(patientName);
	}
}
