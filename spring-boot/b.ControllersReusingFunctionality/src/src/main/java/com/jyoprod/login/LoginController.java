package com.jyoprod.login;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jyoprod.base.ControllerBase;

@RestController
public class LoginController extends ControllerBase {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    //to call use postman and perform get with URL - http://localhost:8080/login?userName=Sandeep&password=san
    @RequestMapping("/login")
    public Login greeting(
    		@RequestParam(value="userName", required=true) String userName,
    		@RequestParam(value="password", required=true) String password
    		) {
        return new Login(counter.incrementAndGet(),
                            String.format(template, userName));
    }
}
