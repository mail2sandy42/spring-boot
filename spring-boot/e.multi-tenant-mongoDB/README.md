multi-tenant-spring-mongodb
===========================

A quick dirty approach for achieving multi tenancy. Better approach is described in the end of this document.

Why?
-----------

If you are having a structure like the following in your mongodb it is pretty bad to handle, since spring-data-mongodb does not support multi tenancy.

```
CUSTOMER_DB_A #
  - collectionA
  - collectionB
  - collectionC
CUSTOMER_DB_B
  - collectionA
  - collectionB
  - collectionC
```
the data structure in these collections is the same, every customer does just have its own database in one mongodb instance.



Conclusion
----------------

I don't think that this is the best approach for following reasons:
- I need to modify the code everytime I need to modify my customer list.
- Fails to adhere to [12 factor principles](http://12factor.net/).MongoDB has not been attached as a resource. So while deploying in any IaaS or PaaS, I cannot make use of mongoDB service they provide.

It only works for quick and dirty service who does not have great importance from the solution point of view.

What is a neat solution then?
--------------------

- Define another service which manages customer specific information. 
- Define this service URL or query URL in application.properties.
When the request hits your controller, query this customer service and get the customer specific mongo configuration. [this class](https://github.com/mail2sandy42/spring-boot/blob/master/spring-boot/f.MongoDB_Jongo_FullyGeneric/src/main/java/com/jyoproducts/controllers/EmployeeServiceImpl.java) shows how you can access external configurations from application.properties.

- Now connect to mongoDB and perform desired operations.

Spring boot allows you to run the jar with different [configuration or profile[(https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-application-property-files) at runtime. So there can be many more ways of doing it right.

