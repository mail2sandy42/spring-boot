package com.jyoprod.interfaces;

public interface IStudyService {

	int getAllStudyCount();

	String getStudyCountFor(String patientName);

}