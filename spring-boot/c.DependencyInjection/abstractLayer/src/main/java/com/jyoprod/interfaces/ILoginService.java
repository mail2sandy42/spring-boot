package com.jyoprod.interfaces;

import com.jyoprod.data.Login;

public interface ILoginService {
	Login performLogin(String userName);
}
