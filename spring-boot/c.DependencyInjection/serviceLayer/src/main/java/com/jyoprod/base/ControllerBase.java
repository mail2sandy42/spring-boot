package com.jyoprod.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.jyoprod.interfaces.ILoginService;
import com.jyoprod.interfaces.IStudyService;

public class ControllerBase {
	@Autowired
	protected IStudyService studyService;
	
	@Autowired
	protected ILoginService loginService;
	
	@Bean
	public IStudyService studyService() {
		return new StudyServiceImpl();
	}
	
	
	@Bean
	public ILoginService loginService() {
		return new LoginServiceImpl();
	}
}
