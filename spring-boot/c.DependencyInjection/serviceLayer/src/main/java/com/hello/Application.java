package com.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//@ComponentScan(basePackageClasses = {LoginController.class,StudyController.class})//You can just use @ComponentScan and avoid explicitly add each controller
@ComponentScan("com.jyoprod")
@EnableAutoConfiguration
public class Application {
   public static void main( String[] args ) {
      SpringApplication.run( Application.class, args );
   }
}