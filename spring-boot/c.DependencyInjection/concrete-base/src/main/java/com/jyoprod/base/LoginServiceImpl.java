package com.jyoprod.base;

import java.util.concurrent.atomic.AtomicLong;

import com.jyoprod.data.Login;
import com.jyoprod.interfaces.ILoginService;


public class LoginServiceImpl implements ILoginService {
	private final AtomicLong counter = new AtomicLong();
	private static final String template = "Hello, %s!";
	
	@Override
	public Login performLogin(String userName) {
		return new Login(counter.incrementAndGet(),
                String.format(template, userName));
	}

}
