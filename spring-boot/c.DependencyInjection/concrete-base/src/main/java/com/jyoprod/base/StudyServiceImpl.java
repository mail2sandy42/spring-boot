package com.jyoprod.base;

import org.springframework.stereotype.Component;
import com.jyoprod.interfaces.IStudyService;

@Component
public class StudyServiceImpl implements IStudyService {

	public StudyServiceImpl() {
		super();
	}


	@Override
	public int getAllStudyCount() {
		return 5;
	}


	@Override
	public String getStudyCountFor(String patientName) {
		return "Patient " + patientName + " has " + 5 + " studies";
	}

}